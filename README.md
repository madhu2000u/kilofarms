# Kilofarms App Task for summer intern 2021
### Architectural Patterns Used:
- MVVM (ViewModel, Repository, Room)
- Single Source of Truth Database paradigm
### Features:
### - Login Screen:
- Phone number and Password Validation
- Signin button
### - Product Listing:
- RecyclerView of all data
- Retrievs from web server and displays
- When offline, displays from local cached database
- Refresh button to refetch from webs server
- Sync with web server button

### - Add Product:
- Entry and option for Name and Categry of product respectively
- Post product to web server
- When offline, option to cache it locallay so that when back online, user can sync with web server by pressing sync button present in product listing page, where cached products will be posted to web server and display will be refreshed.

