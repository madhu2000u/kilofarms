package com.madhu.kilofarms.CreateProduct.Data.Room;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.madhu.kilofarms.CreateProduct.Models.NewProductCacheModel;
import com.madhu.kilofarms.ProductListing.Data.Models.ProductEntity;


import java.util.List;

/**
 * Dao for the offline cache database that holds data of fetched products and stores it as per
 * {@link NewProductCacheModel} schema.
 */
@Dao
public interface OfflineCacheDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertProduct(NewProductCacheModel productEntity);

    @Query("DELETE FROM NewProductCacheModel")
    void deleteAllProductCache();


    @Query("SELECT * FROM NewProductCacheModel")
    List<NewProductCacheModel> getProducts();


}
