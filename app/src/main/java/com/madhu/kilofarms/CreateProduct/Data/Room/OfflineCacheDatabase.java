package com.madhu.kilofarms.CreateProduct.Data.Room;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.madhu.kilofarms.CreateProduct.Models.NewProductCacheModel;
import com.madhu.kilofarms.ProductListing.Data.Models.ProductEntity;


/**
 * Android Room component @Database to define a static instance of database for {@link NewProductCacheModel}
 * access.
 *
 * Singleton class as we want only 1 instance of database for access by whole application.
 */
    @Database(entities = NewProductCacheModel.class, version = 1)
    public abstract class OfflineCacheDatabase extends RoomDatabase {

        private static volatile com.madhu.kilofarms.CreateProduct.Data.Room.OfflineCacheDatabase instance;

        public abstract OfflineCacheDao offlineCacheDao();


        public static com.madhu.kilofarms.CreateProduct.Data.Room.OfflineCacheDatabase getDatabase(Context context){
            if(instance==null){
                instance= Room.databaseBuilder(context.getApplicationContext(), com.madhu.kilofarms.CreateProduct.Data.Room.OfflineCacheDatabase.class, "offlineCacheDatabase")
                        .build();

            }
            return instance;


        }

    }


