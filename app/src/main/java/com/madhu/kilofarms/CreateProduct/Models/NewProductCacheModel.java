package com.madhu.kilofarms.CreateProduct.Models;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
public class NewProductCacheModel {
    /**
     * Member variables
     */
    @PrimaryKey(autoGenerate = true)
    private int id;
    @Expose
    private String skuName;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("skuUnit")
    @Expose
    private String skuUnit;
    @SerializedName("skuCategory")
    @Expose
    private String skuCategory;




    /**
     * Member Functions
     * @return the corresponding variable
     */
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSkuUnit() {
        return skuUnit;
    }

    public void setSkuUnit(String skuUnit) {
        this.skuUnit = skuUnit;
    }

    public String getSkuCategory() {
        return skuCategory;
    }

    public void setSkuCategory(String skuCategory) {
        this.skuCategory = skuCategory;
    }

}
