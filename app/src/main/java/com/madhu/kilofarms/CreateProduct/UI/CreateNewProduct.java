package com.madhu.kilofarms.CreateProduct.UI;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;
import com.kusu.loadingbutton.LoadingButton;
import com.madhu.kilofarms.CreateProduct.Models.NewProductCacheModel;
import com.madhu.kilofarms.R;
import com.madhu.kilofarms.Repository.Repository;

public class CreateNewProduct extends AppCompatActivity {

    private ArrayAdapter<String> spinnerAdapter;
    private String[] categories={"Fruits", "Vegetables", "Others"};
    private Repository repository;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_product);

        Spinner spinner=findViewById(R.id.dropdown_menu);

        spinnerAdapter=new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, categories);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(spinnerAdapter);

        //TODO Empty field check
        TextInputEditText name=findViewById(R.id.skuName);

        Button addProduct=findViewById(R.id.addProduct);

        repository=new Repository(getApplication());


        addProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NewProductCacheModel product=new NewProductCacheModel();
                product.setSkuName(name.getText().toString());
                product.setSkuCategory(spinner.getSelectedItem().toString());
                product.setSkuUnit(getResources().getString(R.string.unit));
                product.setUserId(Repository.getUserId());
                LoadingButton addProductButton=findViewById(R.id.addProduct);
                //addProductButton.showLoading();

                repository.PostToWebService(CreateNewProduct.this, product);

            }
        });



    }
}
