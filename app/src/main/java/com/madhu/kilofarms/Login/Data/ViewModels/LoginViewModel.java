package com.madhu.kilofarms.Login.Data.ViewModels;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

/**
 * ViewModel class that holds data and LiveData bindings for {@link com.madhu.kilofarms.Login.UI.LoginActivity}
 */
public class LoginViewModel extends ViewModel {

    private MutableLiveData<String>validUsername=new MutableLiveData<>();
    private MutableLiveData<Boolean> validPassword =new MutableLiveData<>();
    private MutableLiveData<Boolean> mLoginButtonState=new MutableLiveData<>();


    public LiveData<Boolean> getPasswordValidity(){return validPassword;}
    public LiveData<String> getUsernameValidity(){return validUsername;}
    public LiveData<Boolean> getLoginButtonState(){return mLoginButtonState;}


    public LoginViewModel() {
        mLoginButtonState.setValue(false);
        validPassword.setValue(false);
    }

    /**
     * Checks the validity of both username and password and sets the boolean state to {@link #mLoginButtonState}
     */
    private void setLoginButtonState(){
        Log.d("msg", "Login button state - "+String.valueOf(mLoginButtonState.getValue()));
        mLoginButtonState.setValue(validPassword.getValue() && Boolean.parseBoolean(validUsername.getValue()));
    }

    /**
     * Called whenever text in Username input text box is being changed, validates and sets value
     * of {@link #validUsername}
     *
     * @param username the current value of mUsername in {@link com.madhu.kilofarms.Login.UI.LoginActivity}
     */
    public void usernameChanged(String username){
        if (username==null){validUsername.setValue("Cannot be null");}
        else if(username.contains("+") || username.contains(".") || username.contains("#") || username.contains(",") || username.contains("*")){
            validUsername.setValue("Cannot contain symbol");
        }else if(username.trim().length()>10 || username.trim().length()<10){
            validUsername.setValue("Must be 10 digits");
        }else{validUsername.setValue("true");}
        setLoginButtonState();

    }


    /**
     *
     * @param password set the boolean {@link #validPassword}
     */
    public void passwordChanged(String password) {
        validPassword.setValue(checkPassword(password));
        setLoginButtonState();
    }


    /**
     *
     * @param password check for password validity
     * @return true if password>8 and not null
     */
    private boolean checkPassword(String password){
        return password!=null && password.trim().length()>8;
    }


}
