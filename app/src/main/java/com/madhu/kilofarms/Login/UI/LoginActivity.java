package com.madhu.kilofarms.Login.UI;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.textfield.TextInputEditText;
import com.madhu.kilofarms.Login.Data.ViewModels.LoginViewModel;
import com.madhu.kilofarms.ProductListing.UI.ProductListing;
import com.madhu.kilofarms.R;

public class LoginActivity extends AppCompatActivity{

    private TextInputEditText mUsername;
    private TextInputEditText mPassword;
    private Button mSignInButton;

    private LoginViewModel mLoginViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mUsername=findViewById(R.id.username);
        mPassword=findViewById(R.id.password);
        mSignInButton=findViewById(R.id.loginButton);
        mSignInButton.setEnabled(false);

        mUsername.addTextChangedListener(usernameWatcher);
        mPassword.addTextChangedListener(passwordWatcher);

        mLoginViewModel= new ViewModelProvider(this).get(LoginViewModel.class);
        mLoginViewModel.getPasswordValidity().observe(this, aBoolean -> {
            if(!aBoolean){
                mPassword.setError(getResources().getString(R.string.invalid_password));
            }
            //mSignInButton.setEnabled(aBoolean);     //enable or disable button based on password validity. aBoolean will be true when password is valid
        });

        mLoginViewModel.getUsernameValidity().observe(this, s -> {
            if(!s.equals("true")){
                mUsername.setError(s);
            }

        });

        mLoginViewModel.getLoginButtonState().observe(this, aBoolean -> {
            mSignInButton.setEnabled(aBoolean);

        });

        mSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent productListing=new Intent(getApplicationContext(), ProductListing.class);
                startActivity(productListing);
                finish();

            }
        });


        };

        TextWatcher usernameWatcher=new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mLoginViewModel.usernameChanged(charSequence.toString());


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };

        TextWatcher passwordWatcher=new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                Toast.makeText(LoginActivity.this, charSequence+" i- "+i+" i1- "+i1+" i2- "+i2, Toast.LENGTH_SHORT).show();
//                Log.d(TAG,charSequence+" i- "+i+" i1- "+i1+" i2- "+i2 );
                mLoginViewModel.passwordChanged(charSequence.toString());



            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };














    }

