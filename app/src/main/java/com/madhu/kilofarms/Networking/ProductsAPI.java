package com.madhu.kilofarms.Networking;

import com.madhu.kilofarms.CreateProduct.Models.NewProductCacheModel;

import com.madhu.kilofarms.ProductListing.Data.Models.AllProductsResponse;
import com.madhu.kilofarms.ProductListing.Data.Models.ProductResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ProductsAPI {

    @GET("vegetable")
    Call<ProductResponse> getProduct(@Query("item") String id, @Query("userId") String  userId);

    @GET("vegetable")
    Call<AllProductsResponse> getAllProducts(@Query("item") String id, @Query("userId") String userId);

    @POST("vegetables")
    Call<NewProductCacheModel> postProduct(@Query("item") String id, @Query("userId") String userId, @Body NewProductCacheModel productModel);





}
