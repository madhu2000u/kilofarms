package com.madhu.kilofarms.Networking;

import com.madhu.kilofarms.ProductListing.Data.Models.AllProductsResponse;
import com.madhu.kilofarms.ProductListing.Data.Models.ProductResponse;

import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * This is the Retrofit Class that holds the data and methods for Retrofit to retrieve data from
 * a remote web server using HTTP(S) and process it.
 */
public class RetrofitObj {

    public static String BASE_URL="https://6j57eve9a1.execute-api.us-east-1.amazonaws.com/staging/";
    public static String POST_BASE_URL="https://cc2pruxs38.execute-api.us-east-1.amazonaws.com/staging/";
    private static final int userId=1;
    private Retrofit retrofit;
    private ProductsAPI api;
    private Response<AllProductsResponse> response;
    private Response<ProductResponse> productResponse;

    /**
     * The constructor that instantiates a Retrofit Object using the {@link Retrofit.Builder}
     *
     * @param URL the endpoint to use to build the Retrofit object.
     */
    public RetrofitObj(String URL) {
        retrofit=new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(URL)
                .build();
    }

    /**
     * Method that returns the Retrofit API interface {@link ProductsAPI}
     * @return the interface of the Retrofit Object.
     */
    public ProductsAPI createAPI(){
        return retrofit.create(ProductsAPI.class);
    }



}
