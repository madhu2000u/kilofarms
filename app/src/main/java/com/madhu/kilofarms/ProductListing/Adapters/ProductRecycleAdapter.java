package com.madhu.kilofarms.ProductListing.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.madhu.kilofarms.ProductListing.Data.Models.ProductEntity;
import com.madhu.kilofarms.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ProductRecycleAdapter extends RecyclerView.Adapter<ProductRecycleAdapter.ViewHolder> {
    private List<ProductEntity> products;
    private Context context;

    public ProductRecycleAdapter(List<ProductEntity> products, Context context) {
        //super(DIFFERENCE_CALLBACK);
        this.products = products;
        this.context=context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.product_list_item,parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ProductEntity currentProduct=products.get(products.size()-1-position);
        Picasso.with(context).load(currentProduct.getSkuImage()).into(holder.image);
        holder.name.setText(currentProduct.getSkuName());
        holder.category.setText(currentProduct.getSkuCategory());
        holder.units.setText(currentProduct.getSkuUnit());

    }

    @Override
    public int getItemCount() {
        return products==null?0:products.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView image;
        TextView name, category, units;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            image=itemView.findViewById(R.id.image);
            name=itemView.findViewById(R.id.skuName);
            category=itemView.findViewById(R.id.skuCategory);
            units=itemView.findViewById(R.id.skuUnits);
        }
    }

    public void dataChanged(List<ProductEntity> productEntities){
        products=productEntities;
        notifyDataSetChanged();
    }
}
