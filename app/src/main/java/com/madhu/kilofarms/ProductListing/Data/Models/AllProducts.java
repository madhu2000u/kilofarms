package com.madhu.kilofarms.ProductListing.Data.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * This is the model POJO that holds an object "data" provided in the {@link AllProductsResponse}.
 */
public class AllProducts {

    /**
     * Member Variables
     */
    @SerializedName("skuId")
    @Expose
    private String skuId;
    @SerializedName("skuName")
    @Expose
    private String skuName;

    /**
     * Member Functions
     * @return the corresponding variable
     */
    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }


}
