package com.madhu.kilofarms.ProductListing.Data.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * The web server returns an object in the form when queried for all item:
 * {
 *     "response":"200",
 *     "data":[
 *          {
 *             "skuId": "1616608569.7060363",
 *             "skuName": "Tomato"
 *          },
 *          {
 *  *           "skuId": "1616608569.7060363",
 *  *           "skuName": "Tomato"
 *  *        }
 *     ]
 * }
 *
 * The data field containing a list of all object is of type {@link AllProducts}.
 */
public class AllProductsResponse {

    /**
     * Member Functions
     */
    @SerializedName("response")
    @Expose
    private Integer response;
    @SerializedName("data")
    @Expose
    private List<AllProducts> data = null;


    /**
     * Member Functions
     * @return the corresponding variable
     */
    public Integer getResponse() {
        return response;
    }

    public void setResponse(Integer response) {
        this.response = response;
    }

    public List<AllProducts> getData() {
        return data;
    }

    public void setData(List<AllProducts> data) {
        this.data = data;
    }
}
