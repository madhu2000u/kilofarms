package com.madhu.kilofarms.ProductListing.Data.Models;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NonNls;

import java.util.List;

@Entity(tableName="Products")
public class ProductEntity {

    /**
     * Member Variables
     */
    @NonNull
    @PrimaryKey
    @SerializedName("skuId")
    @Expose
    private String skuId;
    @SerializedName("skuName")
    @Expose
    private String skuName;
    @SerializedName("skuUnit")
    @Expose
    private String skuUnit;
    @SerializedName("skuCategory")
    @Expose
    private String skuCategory;
    @SerializedName("skuImage")
    @Expose
    private String skuImage;


    /**
     * Member Functions
     * @return the corresponding variable
     */
    @NonNull
    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(@NonNull String skuId) {
        this.skuId = skuId;
    }

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public String getSkuUnit() {
        return skuUnit;
    }

    public void setSkuUnit(String skuUnit) {
        this.skuUnit = skuUnit;
    }

    public String getSkuCategory() {
        return skuCategory;
    }

    public void setSkuCategory(String skuCategory) {
        this.skuCategory = skuCategory;
    }

    public String getSkuImage() {
        return skuImage;
    }

    public void setSkuImage(String skuImage) {
        this.skuImage = skuImage;
    }
}

