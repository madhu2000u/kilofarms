package com.madhu.kilofarms.ProductListing.Data.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Individual product details are retrieved using skuId from {@link AllProducts} as shown:
 * {
 *     "response": 200,
 *     "data": [
 *         {
 *             "skuId": "1616608569.7060363",
 *             "skuName": "Tomato",
 *             "skuUnit": "KG",
 *             "skuCategory": "Vegetables",
 *             "skuImage": "https://imageUrl.com/"
 *         }
 *     ]
 * }
 *
 * {@link ProductEntity} class holds the POJO object of each "data" object
 */
public class ProductResponse {
    /**
     * Member Variables
     */
    @SerializedName("response")
    @Expose
    private Integer response;
    @SerializedName("data")
    @Expose
    private List<ProductEntity> data = null;

    /**
     * Member Functions
     * @return the corresponding variable
     */
    public Integer getResponse() {
        return response;
    }

    public void setResponse(Integer response) {
        this.response = response;
    }

    public List<ProductEntity> getData() {
        return data;
    }

    public void setData(List<ProductEntity> data) {
        this.data = data;
    }
}
