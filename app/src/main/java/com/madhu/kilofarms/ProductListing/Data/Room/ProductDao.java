package com.madhu.kilofarms.ProductListing.Data.Room;


import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.madhu.kilofarms.ProductListing.Data.Models.ProductEntity;

import java.util.List;

/**
 * Dao for the Primary database that holds data of fetched products and stores it as per
 * {@link ProductEntity} schema.
 */
@Dao
public interface ProductDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertProduct(ProductEntity product);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateProduct(ProductEntity product);

    @Query("SELECT * FROM Products")
    LiveData<List<ProductEntity>> getAllProducts();




}
