package com.madhu.kilofarms.ProductListing.Data.Room;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.madhu.kilofarms.ProductListing.Data.Models.ProductEntity;

/**
 * Android Room component @Database to define a static instance of database for {@link ProductEntity}
 * access.
 *
 * Singleton class as we want only 1 instance of database for access by whole application.
 */
@Database(entities = ProductEntity.class, version = 1)
public abstract class ProductDatabase extends RoomDatabase {

    private static volatile ProductDatabase instance;

    public abstract ProductDao productDao();

    public static ProductDatabase getDatabase(Context context){
        if(instance==null){
            instance= Room.databaseBuilder(context.getApplicationContext(), ProductDatabase.class, "productDatabase")
                    .build();

        }
        return instance;


    }


}
