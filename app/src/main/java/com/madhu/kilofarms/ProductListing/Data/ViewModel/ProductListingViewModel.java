package com.madhu.kilofarms.ProductListing.Data.ViewModel;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.madhu.kilofarms.ProductListing.Data.Models.ProductEntity;
import com.madhu.kilofarms.Repository.Repository;

import java.util.List;

/**
 * ViewModel class that holds data and LiveData bindings for {@link com.madhu.kilofarms.ProductListing.UI.ProductListing}
 */
public class ProductListingViewModel extends AndroidViewModel {
    private Repository repository;


    public ProductListingViewModel(Application application){
        super(application);
        repository=new Repository(application);

    }

    /**
     * Wrapper for {@link Repository#WebServiceToDb()}
     */
    public void getProductsAndCacheToDB(){
        repository.WebServiceToDb();
    }

    /**
     * Wrapper for {@link Repository#getProducts()}
     * @return LiveData object containing a list of {@link ProductEntity}
     */
    public LiveData<List<ProductEntity>> getAllProducts(){
        return repository.getProducts();

    }

    /**
     * Wrapper for {@link Repository#syncWithServer()}, for UI thread of
     * {@link com.madhu.kilofarms.ProductListing.UI.ProductListing} to access
     */
    public void syncWithServer(){
        repository.syncWithServer();

    }

}
