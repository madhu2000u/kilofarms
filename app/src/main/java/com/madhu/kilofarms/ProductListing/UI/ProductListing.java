package com.madhu.kilofarms.ProductListing.UI;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.madhu.kilofarms.CreateProduct.UI.CreateNewProduct;
import com.madhu.kilofarms.ProductListing.Adapters.ProductRecycleAdapter;
import com.madhu.kilofarms.ProductListing.Data.Models.ProductEntity;
import com.madhu.kilofarms.ProductListing.Data.ViewModel.ProductListingViewModel;
import com.madhu.kilofarms.R;

import java.util.List;

public class ProductListing extends AppCompatActivity {

    private ProductListingViewModel viewModel;
    private RecyclerView productsRecycler;
    private ProductRecycleAdapter adapter;
    private List<ProductEntity> productEntities;
    private SwipeRefreshLayout layout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_listing);
        productsRecycler=findViewById(R.id.productsRecycler);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent AddProduct=new Intent(getApplicationContext(), CreateNewProduct.class);
                startActivity(AddProduct);

            }
        });

        intiRecycler();
        viewModel=new ViewModelProvider(this).get(ProductListingViewModel.class);
        viewModel.getProductsAndCacheToDB();
        viewModel.getAllProducts().observe(this, productEntities -> {       //anonymous function observable replaced with lambda
            if(productEntities!=null){
                adapter.dataChanged(productEntities);

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
            case R.id.sync:
                viewModel.syncWithServer();
                break;
            case R.id.menu_refresh:
                viewModel.getProductsAndCacheToDB();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    private void intiRecycler() {
        adapter=new ProductRecycleAdapter(productEntities, this);
        productsRecycler.setLayoutManager(new LinearLayoutManager(ProductListing.this));
        productsRecycler.setAdapter(adapter);
    }

}
