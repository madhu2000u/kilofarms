package com.madhu.kilofarms.Repository;

import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.WorkerThread;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.LiveData;

import com.madhu.kilofarms.CreateProduct.Data.Room.OfflineCacheDao;
import com.madhu.kilofarms.CreateProduct.Data.Room.OfflineCacheDatabase;
import com.madhu.kilofarms.CreateProduct.Models.NewProductCacheModel;
import com.madhu.kilofarms.Networking.ProductsAPI;
import com.madhu.kilofarms.Networking.RetrofitObj;
import com.madhu.kilofarms.ProductListing.Data.Models.AllProductsResponse;
import com.madhu.kilofarms.ProductListing.Data.Room.ProductDao;
import com.madhu.kilofarms.ProductListing.Data.Room.ProductDatabase;
import com.madhu.kilofarms.ProductListing.Data.Models.ProductEntity;
import com.madhu.kilofarms.ProductListing.Data.Models.ProductResponse;
import com.madhu.kilofarms.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * An abstraction layer for data access from multiple sources. It follows the "Single Source of
 * Truth" paradigm.
 *
 * This repository fetches data form a remote web server and updates it in the local sqlite3 database {@link #mProductDao}
 * and the database spits out changes to ViewModel observer at {@link com.madhu.kilofarms.ProductListing.UI.ProductListing}
 * making the local database as the single source of truth. Hence even in the absence of internet local db is
 * still fetched and displayed.
 *
 * When adding new product to web server using {@link com.madhu.kilofarms.CreateProduct.UI.CreateNewProduct},
 * in the absence of internet, user can choose to store it in another database called {@link #mOfflineDao}. When
 * network is available, user can sync with server by clicking on sync button on the top right corner. The products
 * in this local database will be posted to the server and will finally be deleted.
 *
 */
public class Repository {

    private static final String TAG="Repository";
    private static final String userId="102118035";
    private ProductDao mProductDao;
    private OfflineCacheDao mOfflineDao;
    private List<ProductEntity> productEntities;
    private static volatile Repository instance;
    private Context context;


    public Repository(Application application) {
        //Log.d(TAG, "Repository instance null? - "+Boolean.toString(instance==null));
        ProductDatabase database=ProductDatabase.getDatabase(application);
        mProductDao=database.productDao();

        OfflineCacheDatabase offlineCacheDatabase=OfflineCacheDatabase.getDatabase(application);
        mOfflineDao=offlineCacheDatabase.offlineCacheDao();
        this.context=application;

    }

    /**
     * Creates a Toasts on the UI Thread with the specified message
     * @param message the message to be shown in Toast
     */
    @WorkerThread
    private void workerThread(String message){
        ContextCompat.getMainExecutor(context).execute(()->{
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        });
    }

    public static String getUserId() {
        return userId;
    }

    public LiveData<List<ProductEntity>> getProducts(){
        return mProductDao.getAllProducts();
    }

    /**
     * Post the product to the web server
     *
     * @param context context from which it is called
     * @param newProduct the product {@link ProductEntity} to post to web server
     */
    public void PostToWebService(Context context, NewProductCacheModel newProduct){
        if(checkConnection()){
            DialogInterface.OnClickListener dialogClickListener = (dialog, which) -> {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:

                        class thread extends Thread {
                            @Override
                            public void run() {
                                mOfflineDao.insertProduct(newProduct);
                                workerThread("Added to local");
                            }
                        }
                        new thread().start();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(R.string.noInternet).setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();

            return;

        }
        workerThread("Adding product(s)");
        RetrofitObj retrofitObj= new RetrofitObj(RetrofitObj.POST_BASE_URL);
        ProductsAPI api=retrofitObj.createAPI();

        api.postProduct("all", userId, newProduct).enqueue(new Callback<NewProductCacheModel>() {
            @Override
            public void onResponse(Call<NewProductCacheModel> call, Response<NewProductCacheModel> response) {
                if(response.isSuccessful()){
                    Toast.makeText(context, "Product Added", Toast.LENGTH_SHORT).show();
                }
                WebServiceToDb();   //After adding products to server retrieve to display
            }

            @Override
            public void onFailure(Call<NewProductCacheModel> call, Throwable t) {
                Toast.makeText(context, "Failed to add! Please check you Internet Connection and try again.", Toast.LENGTH_SHORT).show();

            }
        });
    }

    /**
     * This is done to make it into a "Single source of Truth" paradigm, hence all retrieved
     * products from web will be fed into the db and the db will intimate any changes to its LiveData object Observers
     */
    public void WebServiceToDb(){
        if(checkConnection()){
            workerThread("No Internet, showing previews items from local.");
            return;
        }
        workerThread("Syncing...");
        RetrofitObj retrofitObj = new RetrofitObj(RetrofitObj.BASE_URL);
        ProductsAPI api = retrofitObj.createAPI();
        api.getAllProducts("all", String.valueOf(userId)).enqueue(new Callback<AllProductsResponse>() {
            @Override
            public void onResponse(Call<AllProductsResponse> call, Response<AllProductsResponse> response) {
                for (int i = 0; i < response.body().getData().size(); i++) {
                    api.getProduct(response.body().getData().get(i).getSkuId(), userId).enqueue(new Callback<ProductResponse>() {
                        @Override
                        public void onResponse(Call<ProductResponse> call, Response<ProductResponse> response) {
                            //Log.d("msg", "This is the object =" + response.body().getData().get(0).getSkuId());
                            if (response.isSuccessful()) {
                                class thread extends Thread {
                                    @Override
                                    public void run() {
                                        mProductDao.insertProduct(response.body().getData().get(0));
                                    }
                                }
                                new thread().start();
                            }
                        }

                        @Override
                        public void onFailure(Call<ProductResponse> call, Throwable t) {

                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<AllProductsResponse> call, Throwable t) {
            }
        });
    }

    /**
     * When application is offline, if user had added products to offline cache db then this function, when called will
     * post all those to remote server and delete those local products from database {@link #mOfflineDao}
     */
    public void syncWithServer(){   //
        if(checkConnection()){
            Toast.makeText(context, "No internet connection. Please try again later", Toast.LENGTH_SHORT).show();
            return;
        }

        class thread extends Thread {
            @Override
            public void run() {
                List<NewProductCacheModel> productModels=mOfflineDao.getProducts();
                if(productModels.size()==0){
                    workerThread("Nothing in local to sync, everything is up to date.");
                    return;
                }
                for (int i = 0; i < productModels.size(); i++) {
                    PostToWebService(context, productModels.get(i));

                }
                mOfflineDao.deleteAllProductCache();
            }
        }
        new thread().start();

    }

    /**
     * Check for internet connection
     * @return true if no internet & false otherwise
     */
    private boolean checkConnection(){      //Check for internet connection
        ConnectivityManager connectivityManager=(ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info=connectivityManager.getActiveNetworkInfo();
        return info == null || !info.isAvailable() || !info.isConnected();



    }

  }
